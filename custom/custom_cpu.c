#include "fapp.h"
#include "custom_cpu.h"

unsigned long int c_htonl(unsigned long int long_var)
{
	unsigned long int result = C_HTONL(long_var);
    return result;
}

unsigned short int c_htons(unsigned short int short_var)
{
	unsigned short int result = C_HTONS(short_var);
    return result;
}
