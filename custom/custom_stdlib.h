#ifndef _CUSTOM_STDLIB_H_

#define _CUSTOM_STDLIB_H_

/**************************************************************************/ /*!
 * @brief Unsigned integer type representing the size in bytes.
 ******************************************************************************/
typedef unsigned long c_size;

typedef enum
{
    C_OK  = (0), /**< No error.
                     */
    C_ERR = (-1) /**< There is error.
                     */
} c_return_t;


#endif

