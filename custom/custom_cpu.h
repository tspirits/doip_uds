#ifndef _CUSTOM_CPU_H_

#define _CUSTOM_CPU_H_


#define C_HTONL(long_var)    ((((long_var) & 0x000000FFU) << 24U) | (((long_var) & 0x0000FF00U) << 8U) | (((long_var) & 0x00FF0000U) >> 8U) | (((long_var) & 0xFF000000U) >> 24U))
#define C_HTONS(short_var)   ((((short_var) & 0x00FFU) << 8U) | (((short_var) & 0xFF00U) >> 8U))


unsigned long int c_htonl(unsigned long int long_var);
unsigned short int c_htons(unsigned short int short_var);

#endif
