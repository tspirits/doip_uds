#include "custom_socket.h"  /* Include the header (not strictly necessary here) */
#include "fapp.h"


c_socket cus_socket( unsigned short int family, c_socket_type_t type, unsigned long int protocol )
{
	fnet_printf("The Family is %d. \n",family);
	fnet_printf("The Type is %d. \n",type);
	fnet_printf("The protocol is %d. \n",protocol);
	fnet_socket_t sockfd;
	sockfd=fnet_socket(family,type,protocol);

return sockfd;
}

c_return_t cus_socket_bind( c_socket s, const struct c_sockaddr *name, c_size namelen )
{

	fnet_return_t c_error_bind;


	c_error_bind=fnet_socket_bind( s, name,  namelen );
	if(c_error_bind==FNET_OK)
		return C_OK;
	else
		return C_ERR;

}

c_return_t 	cus_socket_listen( c_socket s, c_size backlog )
{

	fnet_return_t c_error_listen;


	c_error_listen=fnet_socket_listen(s,backlog);
	return c_error_listen;



}

c_socket cus_socket_accept( c_socket s, struct c_sockaddr *addr, c_size *addrlen )
{

fnet_socket_t c_sock_accept;
c_sock_accept=fnet_socket_accept(s,addr,addrlen);

return c_sock_accept;



}

signed long int cus_socket_recv( c_socket s, void *buf, c_size len, unsigned int flags )
{
	fnet_size_t s_recieve_size;
	s_recieve_size=fnet_socket_recv(s,buf,len,flags);
	return s_recieve_size;

}

signed long int cus_socket_send( c_socket s, const void *buf, c_size len, unsigned int flags)
{
	  fnet_size_t s_send_size;
	  s_send_size=fnet_socket_send(s,buf,len,flags);
		if(s_send_size==FNET_ERR)
			return C_ERR;
		else
			return s_send_size;


}

signed long int cus_socket_recvfrom( c_socket s, void *buf, c_size len, unsigned int flags, struct c_sockaddr *from, c_size *fromlen )
{
	fnet_size_t c_readfrom_size;
	c_readfrom_size=fnet_socket_recvfrom(s,buf,len,flags,from,fromlen);
	return c_readfrom_size;

}


signed long int cus_socket_sendto( c_socket s, const void *buf, c_size len, unsigned int flags, const struct c_sockaddr *to, c_size tolen )
{

	fnet_size_t s_sendto_size;
	s_sendto_size=fnet_socket_sendto(s,buf,len,flags,to,tolen);
	if(s_sendto_size==FNET_ERR)
		return C_ERR;
	else
		return s_sendto_size;
}
