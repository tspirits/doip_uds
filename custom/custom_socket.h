#ifndef _CUSTOM_SOCKET_H_

#define _CUSTOM_SOCKET_H_

#include "custom_ip6.h"
#include "custom_stdlib.h"

/*
 * IPv4 address family.
*/

#define C_AF_INET     (1U)
/*
 * IPv6 address family.
*/
#define C_AF_INET6    (2U)

/**************************************************************************/ /*!
 * @brief It means to use any network interface.
 ******************************************************************************/
#define C_INADDR_ANY       (unsigned long int)(0x00000000U)



/**************************************************************************/ /*!

 * @brief Socket types.
 ******************************************************************************/
/**************************************************************************/ /*!
 * @brief Protocol numbers and Level numbers for the @ref fnet_socket_setopt()
 * and the @ref fnet_socket_getopt().
 ******************************************************************************/

#define C_SA_DATA_SIZE_IPV4       (sizeof(struct c_in_addr))

typedef enum
{
    C_IPPROTO_IP      = (0), /**< @brief IPv4 options level number
                            *   for @ref fnet_socket_getopt() and @ref fnet_socket_setopt().
                            */
    C_IPPROTO_ICMP    = (1), /**< @brief ICMPv4 protocol number.
                            */
    C_IPPROTO_IGMP    = (2), /**< @brief IGMP protocol number.
                            */
    C_IPPROTO_TCP     = (6), /**< @brief TCP protocol number; TCP options level number
                            *   for @ref fnet_socket_getopt() and @ref fnet_socket_setopt().
                            */
    C_IPPROTO_UDP     = (17),/**< @brief UDP protocol number.
                            */
    C_IPPROTO_IPV6    = (41), /**< @brief IPv6 options level number
                            *    for @ref fnet_socket_getopt() and @ref fnet_socket_setopt().
                            */
    C_IPPROTO_ICMPV6  = (58),/**< @brief ICMPv6 protocol number.
                            */
    C_SOL_SOCKET      = (255255)  /**< @brief Socket options level number for
                                 * @ref fnet_socket_getopt() and @ref fnet_socket_setopt().
                                 */
} c_protocol_t;


typedef enum
{
    C_SOCK_UNSPEC = (0U),  /**< @brief Unspecified socket type.
                         */
    C_SOCK_STREAM = (1U),  /**< @brief Stream socket.@n
                         * Provides reliable, two-way, connection-based
                         * byte stream. It corresponds to the TCP protocol
                         */
    C_SOCK_DGRAM  = (2U),  /**< @brief Datagram socket.@n
                         * Provides unreliable, connectionless datagrams.
                         * It corresponds to the UDP protocol.
                         */
    C_SOCK_RAW    = (3U)   /**< @brief Raw socket.@n
                         * Raw sockets allow an application to have direct access to
                         * lower-level communication protocols.
                         * Raw sockets are intended to take advantage of some protocol feature
                         * that is not directly accessible through a normal interface,
                         * or to build new protocols on top of existing low-level protocols.@n
                         * It can be enabled by the @ref FNET_CFG_RAW option.
                         */
} c_socket_type_t;

/**************************************************************************/ /*!
 * @brief Socket descriptor.
 ******************************************************************************/
typedef void* c_socket;

/**************************************************************************/ /*!
 * @brief IPv6 Socket address structure.
 *
 * @see sockaddr
 *
 * To make manipulation of the @ref sockaddr structure easier, the TCP/IPv6 stack
 * also defines the equivalent structure @ref sockaddr_in6
 * ("in" means "Internet").
 ******************************************************************************/


struct c_in_addr
{
	unsigned long int c_s_addr; /**< @brief 32-bit IPv4 address (in network byte order).
                            */
};

struct c_sockaddr
{
	unsigned short int       sa_family;      /**< @brief Address family. Specifies the
                                            * address family, to which the address belongs. @n
                                            * It is defined by @ref fnet_address_family_t.*/
	unsigned short int       sa_port;        /**< @brief 16-bit port number used to
                                            * demultiplex the transport-level messages
                                            * (in network byte order).*/
	unsigned long int        sa_scope_id;    /**< @brief Scope zone index, defining network interface.*/
	unsigned char            sa_data[C_SA_DATA_SIZE_IPV4];/**< @brief Address value. For the TCP/IP stack,
                                            * it contains the destination address and port
                                            * number for a socket.*/
};


struct c_sockaddr_in
{
	unsigned short int   c_sin_family;     /**=< @brief Specifies the address family. @n
                                            * It must ne set to @ref AF_INET.*/
	unsigned short int   c_sin_port;       /**< @brief 16-bit port number used to
                                            * demultiplex the transport-level messages
                                            * (in network byte order).*/
	unsigned long int     c_sin_scope_id;   /**< @brief Scope zone index, defining network interface.*/
    struct c_in_addr      c_sin_addr;       /**< @brief 32-bit internet address.*/
};

struct c_in6_addr
{
c_ip6_addr_t c_s6_addr;        /**< @brief 128-bit IPv6 address.*/
};



struct c_sockaddr_in6
{
	unsigned short int  c_sin6_family;    /**< @brief Specifies the address family. @n
                                            * It must ne set to @ref AF_INET6. */
	unsigned short int  c_sin6_port;      /**< @brief 16-bit port number used to
                                            * demultiplex the transport-level messages
                                            * (in network byte order).*/
	unsigned long int   c_sin6_scope_id;  /**< @brief Scope zone index, defining network interface.*/
    struct c_in6_addr   c_sin6_addr;      /**< @brief 128-bit IPv6 internet address.*/
};

c_socket cus_socket( unsigned short int family, c_socket_type_t type, unsigned long int protocol );

c_return_t cus_socket_bind( c_socket s, const struct c_sockaddr *name, c_size namelen );

c_return_t 	cus_socket_listen( c_socket s, c_size backlog );

c_socket cus_socket_accept( c_socket s, struct c_sockaddr *addr, c_size *addrlen );

signed long int cus_socket_recv( c_socket s, void *buf, c_size len, unsigned int flags );

signed long int cus_socket_send( c_socket s, const void *buf, c_size len, unsigned int flags );

signed long int cus_socket_recvfrom( c_socket s, void *buf, c_size len, unsigned int flags, struct c_sockaddr *from, c_size *fromlen );
signed long int cus_socket_sendto( c_socket s, const void *buf, c_size len, unsigned int flags, const struct c_sockaddr *to, c_size tolen );


#endif

