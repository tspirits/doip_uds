#ifndef _CUSTOM_IP6_H_

#define _CUSTOM_IP6_H_




/* 128-bit IP6 address */
typedef union
{
	unsigned char   addr[16];
	unsigned short int  addr16[8];
	unsigned long int addr32[4];
}c_ip6_addr_t;

#endif
