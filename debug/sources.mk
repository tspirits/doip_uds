################################################################################
# Automatically-generated file. Do not edit!
################################################################################

ELF_SRCS := 
O_SRCS := 
CPP_SRCS := 
C_UPPER_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
OBJ_SRCS := 
ASM_SRCS := 
CXX_SRCS := 
C++_SRCS := 
CC_SRCS := 
C++_DEPS := 
OBJS := 
C_DEPS := 
ASM_DEPS := 
CC_DEPS := 
SECONDARY_FLASH := 
CPP_DEPS := 
CXX_DEPS := 
C_UPPER_DEPS := 
S_UPPER_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
startup \
source \
fnet_stack/stack \
fnet_stack/services/tls \
fnet_stack/services/tftp \
fnet_stack/services/telnet \
fnet_stack/services/sntp \
fnet_stack/services/shell \
fnet_stack/services/serial \
fnet_stack/services/poll \
fnet_stack/services/ping \
fnet_stack/services/mdns \
fnet_stack/services/llmnr \
fnet_stack/services/link \
fnet_stack/services/http \
fnet_stack/services/fs \
fnet_stack/services/flash \
fnet_stack/services/dns \
fnet_stack/services/dhcp \
fnet_stack/services/autoip \
fnet_stack/port/cpu/mk \
fnet_stack/port/cpu \
fnet_stack/port/cpu/common \
fnet_application \
custom \

