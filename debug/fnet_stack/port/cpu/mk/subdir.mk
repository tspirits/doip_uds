################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk.c \
D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_cache.c \
D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_eth.c \
D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_flash.c \
D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_isr.c \
D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_isr_inst.c \
D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_serial.c \
D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_timer.c 

S_UPPER_SRCS += \
D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_low.S 

OBJS += \
./fnet_stack/port/cpu/mk/fnet_mk.o \
./fnet_stack/port/cpu/mk/fnet_mk_cache.o \
./fnet_stack/port/cpu/mk/fnet_mk_eth.o \
./fnet_stack/port/cpu/mk/fnet_mk_flash.o \
./fnet_stack/port/cpu/mk/fnet_mk_isr.o \
./fnet_stack/port/cpu/mk/fnet_mk_isr_inst.o \
./fnet_stack/port/cpu/mk/fnet_mk_low.o \
./fnet_stack/port/cpu/mk/fnet_mk_serial.o \
./fnet_stack/port/cpu/mk/fnet_mk_timer.o 

C_DEPS += \
./fnet_stack/port/cpu/mk/fnet_mk.d \
./fnet_stack/port/cpu/mk/fnet_mk_cache.d \
./fnet_stack/port/cpu/mk/fnet_mk_eth.d \
./fnet_stack/port/cpu/mk/fnet_mk_flash.d \
./fnet_stack/port/cpu/mk/fnet_mk_isr.d \
./fnet_stack/port/cpu/mk/fnet_mk_isr_inst.d \
./fnet_stack/port/cpu/mk/fnet_mk_serial.d \
./fnet_stack/port/cpu/mk/fnet_mk_timer.d 

S_UPPER_DEPS += \
./fnet_stack/port/cpu/mk/fnet_mk_low.d 


# Each subdirectory must supply rules for building sources it contributes
fnet_stack/port/cpu/mk/fnet_mk.o: D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/port/cpu/mk/fnet_mk.d" -MT"fnet_stack/port/cpu/mk/fnet_mk.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/port/cpu/mk/fnet_mk_cache.o: D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_cache.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/port/cpu/mk/fnet_mk_cache.d" -MT"fnet_stack/port/cpu/mk/fnet_mk_cache.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/port/cpu/mk/fnet_mk_eth.o: D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_eth.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/port/cpu/mk/fnet_mk_eth.d" -MT"fnet_stack/port/cpu/mk/fnet_mk_eth.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/port/cpu/mk/fnet_mk_flash.o: D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_flash.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/port/cpu/mk/fnet_mk_flash.d" -MT"fnet_stack/port/cpu/mk/fnet_mk_flash.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/port/cpu/mk/fnet_mk_isr.o: D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_isr.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/port/cpu/mk/fnet_mk_isr.d" -MT"fnet_stack/port/cpu/mk/fnet_mk_isr.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/port/cpu/mk/fnet_mk_isr_inst.o: D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_isr_inst.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/port/cpu/mk/fnet_mk_isr_inst.d" -MT"fnet_stack/port/cpu/mk/fnet_mk_isr_inst.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/port/cpu/mk/fnet_mk_low.o: D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_low.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -x assembler-with-cpp -DDEBUG -DFNET_CFG_COMP_GNUC=1 -D__STARTUP_CLEAR_BSS -DFNET_CFG_CPU_MK64FN1=1 -I../../../../../src/shell -I../../../../../../fnet_stack -mapcs -MMD -MP -MF"fnet_stack/port/cpu/mk/fnet_mk_low.d" -MT"fnet_stack/port/cpu/mk/fnet_mk_low.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/port/cpu/mk/fnet_mk_serial.o: D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_serial.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/port/cpu/mk/fnet_mk_serial.d" -MT"fnet_stack/port/cpu/mk/fnet_mk_serial.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/port/cpu/mk/fnet_mk_timer.o: D:/Software/FNET\ 3.9.0/fnet_stack/port/cpu/mk/fnet_mk_timer.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/port/cpu/mk/fnet_mk_timer.d" -MT"fnet_stack/port/cpu/mk/fnet_mk_timer.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


