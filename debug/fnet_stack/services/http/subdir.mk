################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
D:/Software/FNET\ 3.9.0/fnet_stack/services/http/fnet_http.c \
D:/Software/FNET\ 3.9.0/fnet_stack/services/http/fnet_http_auth.c \
D:/Software/FNET\ 3.9.0/fnet_stack/services/http/fnet_http_cgi.c \
D:/Software/FNET\ 3.9.0/fnet_stack/services/http/fnet_http_get.c \
D:/Software/FNET\ 3.9.0/fnet_stack/services/http/fnet_http_post.c \
D:/Software/FNET\ 3.9.0/fnet_stack/services/http/fnet_http_ssi.c 

OBJS += \
./fnet_stack/services/http/fnet_http.o \
./fnet_stack/services/http/fnet_http_auth.o \
./fnet_stack/services/http/fnet_http_cgi.o \
./fnet_stack/services/http/fnet_http_get.o \
./fnet_stack/services/http/fnet_http_post.o \
./fnet_stack/services/http/fnet_http_ssi.o 

C_DEPS += \
./fnet_stack/services/http/fnet_http.d \
./fnet_stack/services/http/fnet_http_auth.d \
./fnet_stack/services/http/fnet_http_cgi.d \
./fnet_stack/services/http/fnet_http_get.d \
./fnet_stack/services/http/fnet_http_post.d \
./fnet_stack/services/http/fnet_http_ssi.d 


# Each subdirectory must supply rules for building sources it contributes
fnet_stack/services/http/fnet_http.o: D:/Software/FNET\ 3.9.0/fnet_stack/services/http/fnet_http.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/services/http/fnet_http.d" -MT"fnet_stack/services/http/fnet_http.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/services/http/fnet_http_auth.o: D:/Software/FNET\ 3.9.0/fnet_stack/services/http/fnet_http_auth.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/services/http/fnet_http_auth.d" -MT"fnet_stack/services/http/fnet_http_auth.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/services/http/fnet_http_cgi.o: D:/Software/FNET\ 3.9.0/fnet_stack/services/http/fnet_http_cgi.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/services/http/fnet_http_cgi.d" -MT"fnet_stack/services/http/fnet_http_cgi.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/services/http/fnet_http_get.o: D:/Software/FNET\ 3.9.0/fnet_stack/services/http/fnet_http_get.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/services/http/fnet_http_get.d" -MT"fnet_stack/services/http/fnet_http_get.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/services/http/fnet_http_post.o: D:/Software/FNET\ 3.9.0/fnet_stack/services/http/fnet_http_post.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/services/http/fnet_http_post.d" -MT"fnet_stack/services/http/fnet_http_post.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_stack/services/http/fnet_http_ssi.o: D:/Software/FNET\ 3.9.0/fnet_stack/services/http/fnet_http_ssi.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_stack/services/http/fnet_http_ssi.d" -MT"fnet_stack/services/http/fnet_http_ssi.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


