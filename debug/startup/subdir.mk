################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
D:/Software/FNET\ 3.9.0/fnet_demos/common/startup/MK64F12/clock_config.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/startup/MK64F12/fsl_clock.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/startup/MK64F12/fsl_smc.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/startup/MK64F12/system_MK64F12.c 

S_UPPER_SRCS += \
D:/Software/FNET\ 3.9.0/fnet_demos/common/startup/MK64F12/gcc/startup_MK64F12.S 

OBJS += \
./startup/clock_config.o \
./startup/fsl_clock.o \
./startup/fsl_smc.o \
./startup/startup_MK64F12.o \
./startup/system_MK64F12.o 

C_DEPS += \
./startup/clock_config.d \
./startup/fsl_clock.d \
./startup/fsl_smc.d \
./startup/system_MK64F12.d 

S_UPPER_DEPS += \
./startup/startup_MK64F12.d 


# Each subdirectory must supply rules for building sources it contributes
startup/clock_config.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/startup/MK64F12/clock_config.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"startup/clock_config.d" -MT"startup/clock_config.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

startup/fsl_clock.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/startup/MK64F12/fsl_clock.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"startup/fsl_clock.d" -MT"startup/fsl_clock.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

startup/fsl_smc.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/startup/MK64F12/fsl_smc.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"startup/fsl_smc.d" -MT"startup/fsl_smc.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

startup/startup_MK64F12.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/startup/MK64F12/gcc/startup_MK64F12.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -x assembler-with-cpp -DDEBUG -DFNET_CFG_COMP_GNUC=1 -D__STARTUP_CLEAR_BSS -DFNET_CFG_CPU_MK64FN1=1 -I../../../../../src/shell -I../../../../../../fnet_stack -mapcs -MMD -MP -MF"startup/startup_MK64F12.d" -MT"startup/startup_MK64F12.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

startup/system_MK64F12.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/startup/MK64F12/system_MK64F12.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"startup/system_MK64F12.d" -MT"startup/system_MK64F12.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


