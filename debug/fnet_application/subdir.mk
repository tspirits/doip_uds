################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_autoip.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_bench.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_dhcp.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_dns.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_fs.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_fs_image.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_http.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_link.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_llmnr.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_mdns.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_mem.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_netif.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_params.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_ping.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_setget.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_sntp.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_telnet.c \
D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_tftp.c 

OBJS += \
./fnet_application/fapp.o \
./fnet_application/fapp_autoip.o \
./fnet_application/fapp_bench.o \
./fnet_application/fapp_dhcp.o \
./fnet_application/fapp_dns.o \
./fnet_application/fapp_fs.o \
./fnet_application/fapp_fs_image.o \
./fnet_application/fapp_http.o \
./fnet_application/fapp_link.o \
./fnet_application/fapp_llmnr.o \
./fnet_application/fapp_mdns.o \
./fnet_application/fapp_mem.o \
./fnet_application/fapp_netif.o \
./fnet_application/fapp_params.o \
./fnet_application/fapp_ping.o \
./fnet_application/fapp_setget.o \
./fnet_application/fapp_sntp.o \
./fnet_application/fapp_telnet.o \
./fnet_application/fapp_tftp.o 

C_DEPS += \
./fnet_application/fapp.d \
./fnet_application/fapp_autoip.d \
./fnet_application/fapp_bench.d \
./fnet_application/fapp_dhcp.d \
./fnet_application/fapp_dns.d \
./fnet_application/fapp_fs.d \
./fnet_application/fapp_fs_image.d \
./fnet_application/fapp_http.d \
./fnet_application/fapp_link.d \
./fnet_application/fapp_llmnr.d \
./fnet_application/fapp_mdns.d \
./fnet_application/fapp_mem.d \
./fnet_application/fapp_netif.d \
./fnet_application/fapp_params.d \
./fnet_application/fapp_ping.d \
./fnet_application/fapp_setget.d \
./fnet_application/fapp_sntp.d \
./fnet_application/fapp_telnet.d \
./fnet_application/fapp_tftp.d 


# Each subdirectory must supply rules for building sources it contributes
fnet_application/fapp.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp.d" -MT"fnet_application/fapp.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_autoip.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_autoip.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_autoip.d" -MT"fnet_application/fapp_autoip.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_bench.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_bench.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_bench.d" -MT"fnet_application/fapp_bench.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_dhcp.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_dhcp.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_dhcp.d" -MT"fnet_application/fapp_dhcp.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_dns.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_dns.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_dns.d" -MT"fnet_application/fapp_dns.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_fs.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_fs.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_fs.d" -MT"fnet_application/fapp_fs.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_fs_image.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_fs_image.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_fs_image.d" -MT"fnet_application/fapp_fs_image.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_http.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_http.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_http.d" -MT"fnet_application/fapp_http.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_link.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_link.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_link.d" -MT"fnet_application/fapp_link.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_llmnr.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_llmnr.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_llmnr.d" -MT"fnet_application/fapp_llmnr.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_mdns.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_mdns.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_mdns.d" -MT"fnet_application/fapp_mdns.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_mem.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_mem.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_mem.d" -MT"fnet_application/fapp_mem.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_netif.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_netif.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_netif.d" -MT"fnet_application/fapp_netif.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_params.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_params.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_params.d" -MT"fnet_application/fapp_params.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_ping.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_ping.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_ping.d" -MT"fnet_application/fapp_ping.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_setget.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_setget.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_setget.d" -MT"fnet_application/fapp_setget.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_sntp.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_sntp.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_sntp.d" -MT"fnet_application/fapp_sntp.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_telnet.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_telnet.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_telnet.d" -MT"fnet_application/fapp_telnet.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fnet_application/fapp_tftp.o: D:/Software/FNET\ 3.9.0/fnet_demos/common/fnet_application/fapp_tftp.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"fnet_application/fapp_tftp.d" -MT"fnet_application/fapp_tftp.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


