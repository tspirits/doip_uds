################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../custom/custom_add.c \
../custom/custom_cpu.c \
../custom/custom_serial.c \
../custom/custom_socket.c 

OBJS += \
./custom/custom_add.o \
./custom/custom_cpu.o \
./custom/custom_serial.o \
./custom/custom_socket.o 

C_DEPS += \
./custom/custom_add.d \
./custom/custom_cpu.d \
./custom/custom_serial.d \
./custom/custom_socket.d 


# Each subdirectory must supply rules for building sources it contributes
custom/%.o: ../custom/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -flto -Wall  -g -DDEBUG -DFNET_CFG_COMP_GNUC=1 -DCPU_MK64FN1M0VMD12 -DFRDM_K64F -DFREEDOM -DFNET_CFG_CPU_MK64FN1=1 -DFNET_CFG_CPU_SERIAL_PORT_DEFAULT=0u -I../../../../../common/startup/CMSIS -I../../../../../common/startup -I../.. -I../../../../../src/shell -I../../../../../../fnet_stack -I../../../../../common/fnet_application -I"D:\Software\FNET 3.9.0\fnet_demos\boards\frdmk64f\shell\kds\custom" -std=gnu99 -mapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


